# The contents of this repository have moved

Please refer to the [Redpoint Plugin Example Projects](https://gitlab.com/redpointgames/examples) repository instead. If you got sent here by a documentation link, please let us know in [support](https://redpoint.games/support/).